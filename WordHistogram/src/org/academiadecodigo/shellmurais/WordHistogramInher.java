package org.academiadecodigo.shellmurais;

import java.util.*;

public class WordHistogramInher extends HashMap<String, Integer> implements Iterable<String>{

    public WordHistogramInher(String string){
        indWord(string);
    }
    public void indWord(String string){
        String[] str = string.split(" ");
        for(String str2 : str) {
            if (containsKey(str2)) {
                put(str2, get(str2) + 1);
            }
            else {
                put(str2, 1);
            }
        }
    }
    /*public int size(){
        return size();
    }*/

    /*public Integer get(String key){
        return this.get(key);
    }*/

    public Iterator<String> iterator() {
        return this.keySet().iterator();
    }
}
