package org.academiadecodigo.shellmurais;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class WordHistogramComp implements Iterable<String>{

     private Map<String,Integer> map = new HashMap<>();

    public WordHistogramComp(String string){
        indWord(string);
    }
    public void indWord(String string){
        String[] str = string.split(" ");
        for(String str2 : str) {
            if (map.containsKey(str2)) {
                map.put(str2, map.get(str2) + 1);
            }
            else {
                map.put(str2, 1);
            }
        }
    }

    public int size(){
        return map.size();
    }

    public Integer get(String key){
        return map.get(key);
    }

    @Override
    public Iterator<String> iterator() {
        return map.keySet().iterator();
    }
}
