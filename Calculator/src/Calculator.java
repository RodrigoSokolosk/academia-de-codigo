public class Calculator {

    public String color;
    public String brand;

    public Calculator(String color, String brand){

        this.color = color;
        this.brand = brand;
    }

    /*public int Add(int n1, int n2) {
        return n1 + n2;
    }*/

    public double Add(double n1, double n2){
        return n1 + n2;
    }

    /*public int Sub(int n1, int n2) {
        return n1 - n2;
    }*/

    public double Sub(double n1, double n2) {
        return n1 - n2;
    }

    /*public int Product(int n1, int n2) {
        return n1 * n2;
    }*/

    public double Product(double n1, double n2){
        return n1 * n2;
    }

    /*public int Division(int n1, int n2) {
        return n1 / n2;
    }*/
    public double Division(double n1, double n2){
        return n1 / n2;
    }

    public double Percent(double n){
        return n/100;

    }
}