import java.text.DecimalFormat;

public class StartCalculator {

    public static void main(String[] args){

        Calculator calculator = new Calculator("black", "Texas");
        //calculator.color = "Yellow";
        //calculator.brand = "Casio";

        Calculator anotherCalculator = new Calculator("blue", "outra");
        anotherCalculator.color = "Blue";
        anotherCalculator.brand = "Texas";

        System.out.println("My first calculator is " + calculator.brand + " and is " + calculator.color);

        double resultAdd = calculator.Add(2, 3.5);
        double resultProduct = calculator.Product(3, 5.5);
        double resultSub = calculator.Sub(4, 8);
        double resultDiv = calculator.Division(10, 3);
        DecimalFormat df = new DecimalFormat(("#.##"));
        System.out.println(resultAdd);
        System.out.println(resultProduct);
        System.out.println(resultSub);
        System.out.println(df.format(resultDiv));
        System.out.println(calculator.Percent(resultProduct));

    }


}
