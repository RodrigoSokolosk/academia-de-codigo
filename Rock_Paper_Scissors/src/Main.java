public class Main {

    public static void main(String[] args) {

        GameManager gameManager = new GameManager();
        //GameManager g = new GameManager(rounds, new Player("P1"), new Player("P2"));      //podemos passar os argumentos direto na invocação do metodo construtor

        Player player[] = new Player[2];
        player[0] = new Player("Rodrigo");
        player[1] = new Player("Computer");
        int rounds = 3;

        System.out.println(player[0].getName() + " VS " + player[1].getName() + " in Best of " + rounds);
        System.out.println();

        gameManager.startGame(player[0].getName(), player[1].getName(), rounds);



    }
}
