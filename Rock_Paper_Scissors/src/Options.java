public enum Options {
    ROCK("Rock"),
    PAPER("Paper"),
    SCISSORS("Scissors");

    private String symbol;

    Options (String symbol){
        this.symbol = symbol;
    }
    public String getSymbol(){
        return this.symbol;
    }
}
