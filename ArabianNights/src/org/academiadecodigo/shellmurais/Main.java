package org.academiadecodigo.shellmurais;

public class Main {
    public static void main(String[] args) {

        MagicLamp magicLamp = new MagicLamp("power shell", 3);
        MagicLamp lamp = new MagicLamp("unix", 5);

        System.out.println(magicLamp.getMagicLampName());
        System.out.println(magicLamp.getMaxGenies());
        magicLamp.recyclableDemon();
        magicLamp.rubbed("A");
        System.out.println("You can create more " + magicLamp.getMaxGenies() + " Genies in your MagicLamp!!");
        System.out.println("------------");
        magicLamp.rubbed("B");
        System.out.println("------------");
        magicLamp.rubbed("C");
        System.out.println("------------");
        magicLamp.rubbed("D");
        System.out.println("------------");
        System.out.println("You can create more " + magicLamp.getMaxGenies() + " Genies in your MagicLamp!!");
        magicLamp.recyclableDemon();
        magicLamp.rubbed("E");
        System.out.println("------------");
        magicLamp.recyclableDemon();
        System.out.println("You can create more " + magicLamp.getMaxGenies() + " Genies in your MagicLamp!!");
        magicLamp.rubbed("F");
        System.out.println("------------");
        System.out.println("You can create more " + magicLamp.getMaxGenies() + " Genies in your MagicLamp!!");
        magicLamp.rubbed("G");
        System.out.println("------------");
        System.out.println("You can create more " + magicLamp.getMaxGenies() + " Genies in your MagicLamp!!");
        System.out.println("You already recharge your MagicLamp " + magicLamp.getRecharge() + " times!!!");
        System.out.println("*******************");
        System.out.println("The lamps " + magicLamp.getMagicLampName() + " are equal to "
        + lamp.getMagicLampName() + " = " + magicLamp.compareLamp(lamp));
    }

}
