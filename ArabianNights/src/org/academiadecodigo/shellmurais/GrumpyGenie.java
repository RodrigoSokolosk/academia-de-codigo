package org.academiadecodigo.shellmurais;

public class GrumpyGenie extends Genie {

    private String name;

    public GrumpyGenie(String name){
        super(name);
        this.name = name;
    }
    @Override
    public boolean hasWishesToGrant(){
        return getWishesAtTime() == 0;
    }
    @Override
    public void rubbed() {

        System.out.println("You have " + getMaxWishes() + " wishes and can do " + getWishesAtTime() + " per time");
        System.out.println("Your wishes over, Because i'm Grumpy!!!");
    }

}
