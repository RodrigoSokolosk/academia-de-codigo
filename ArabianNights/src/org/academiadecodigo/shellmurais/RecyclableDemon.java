package org.academiadecodigo.shellmurais;

public class RecyclableDemon extends Genie{

    private String name;
    private boolean recycled;

    public RecyclableDemon(String name){
        super(name);
        this.name = name;
        this.recycled = false;
    }

    public boolean isRecycled(){
        return recycled;
    }
    public void recycle(){
        this.recycled = true;
    }

    @Override
    public boolean hasWishesToGrant(){
        return !recycled;
    }
    @Override
    public void rubbed() {
        while(true) {
            if (getMaxWishes() > 0) {
                System.out.println("You have " + getMaxWishes() + " wishes and can do " + getWishesAtTime() + " per time");
                setMaxWishes();
            }
            if(getMaxWishes() == 0){
                System.out.println("You can do All wishes you want!!");
                return;
            }

        }
    }
}
