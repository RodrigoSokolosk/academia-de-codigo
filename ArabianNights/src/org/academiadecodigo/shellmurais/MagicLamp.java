package org.academiadecodigo.shellmurais;

public class MagicLamp {
    private String magicLampName;
    private int maxGenies;
    //private RecyclableDemon demon;
    private int pos;
    private int recharge = 0;
    private int demons = 0;

    public MagicLamp(String magicLampName, int maxGenies){
        this.maxGenies = maxGenies;
        this.magicLampName = magicLampName;
        this.pos = maxGenies;
    }

    public String getMagicLampName(){
        return this.magicLampName;
    }
    public int getMaxGenies(){
        return this.maxGenies;
    }
    public int getRecharge(){
        return  this.recharge;
    }

    public void rubbed(String name) {

            if (maxGenies != 0 && maxGenies%2 == 0) {
                FriendlyGenie friendlyGenie = new FriendlyGenie(name);
                System.out.println("You make a new genie, " + name + " is his name!!");
                maxGenies--;
                friendlyGenie.rubbed();
                return;
            }
            if (maxGenies !=0 && maxGenies%2 == 1){
                GrumpyGenie grumpyGenie = new GrumpyGenie(name);
                System.out.println("You make a new genie, " + name + " is his name!!");
                maxGenies--;
                grumpyGenie.rubbed();
                return;
            }
            if(maxGenies == 0) {
                RecyclableDemon demon = new RecyclableDemon(name);
                System.out.println("You make a Demon, " + name + " is his name!!");
                demon.rubbed();
                demons++;
            }
        }

    public void recyclableDemon() {
        if (demons > 0) {
            System.out.println("Recyclable Demon!!!");
            this.maxGenies = pos;
            recharge++;
            System.out.println("Recharge Lamp!!!");
            System.out.println("maxGenies is " + maxGenies);
            demons--;
        }
        else{
            System.out.println("You don't have RecyclableDemons to recycle!!!");
        }
    }

    public boolean compareLamp(MagicLamp lamp){

        System.out.println("The Magic Lamp " + magicLampName + " can do " + this.maxGenies + " Genies, " + "You recharge this Lamp " +
                this.recharge + " Times, and the maximum Genius capacity is " + this.pos + " Genies!!!");
        System.out.println("The Magic Lamp " + lamp.magicLampName + " can do " + lamp.maxGenies + " Genies, " + "You recharge this Lamp " +
                lamp.recharge + " Times, and the maximum Genius capacity is " + lamp.pos + " Genies!!!");
        return this.recharge == lamp.recharge
                && this.pos == lamp.pos
                && this.maxGenies == lamp.maxGenies;

    }
    /*@Override
    public String toString() {
        return "Capacity{" +
                "max Genies for this Lamp='" + pos + '\'' +
                ", You can build=" + maxGenies + ", You recharge this lamp " + recharge + " times " +
                '}';
    }*/

}
