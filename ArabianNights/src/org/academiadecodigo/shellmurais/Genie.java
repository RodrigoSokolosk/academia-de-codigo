package org.academiadecodigo.shellmurais;

public class Genie {
    private String name;
    private int maxWishes;
    private int wishesAtTime;

    public Genie(String name){
        this.name = name;
        this.maxWishes = 3;
        this.wishesAtTime =0;
    }

    public int getMaxWishes(){
        return maxWishes;
    }
    public int getWishesAtTime(){
        return wishesAtTime;
    }
    public void setMaxWishes(){
        this.maxWishes--;
    }

    public void grantWishes(){
        if(!hasWishesToGrant()){
            System.out.println(this + " Wont grant any more wishes!!!");
            return;
        }
        System.out.println(this + "Has granted your Wish!!!");
        wishesAtTime++;
    }

    public boolean hasWishesToGrant(){
        return wishesAtTime<maxWishes;
    }
    public void rubbed() {
        while(true) {
            if (maxWishes > 0) {
                System.out.println("You have " + maxWishes + " wishes and can do " + wishesAtTime + " per time");
                maxWishes--;
            }
            if(maxWishes ==0){
                System.out.println("Your wishes over!!");
                return;
            }
        }
    }

}
/*
* private int maxWishes;
* private int wishesGranted;
*
* public Genie(int maxWishes){
* this.wishesgranted = 0;
* this.maxWishes = maxWishes;
* }
*
* public int getNumberofWisheGranted(){return  wishesGranted;}
*
*
* */
