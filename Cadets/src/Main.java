public class Main {

    public static void main(String[] args) {

        String[] name = {"Joao", "Rodrigo", "Antonio", "Diana", "Andre"};
        String[] pitch = {"abcd", "vgvg", "bnuibu", "nbubu", "zzzzz"};

        int cadetName = 5;
        Cadets codeCadets[] = new Cadets[cadetName];

        Cadets newCadet = new Cadets("a", "b");

        for (int i = 0; i < cadetName; i++) {
            codeCadets[i] = new Cadets(name[0], pitch[0]);
            System.out.println("My name's " + name[i] + " and i'm " + pitch[i]);

        }

        double newPitch = Math.random() * name.length;
        int n = (int) newPitch;
        String newName = name[n];
        System.out.println("Cadet " + newName + " i'm " + newCadet.FunnyPitch());
    }
}
