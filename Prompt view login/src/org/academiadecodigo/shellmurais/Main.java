package org.academiadecodigo.shellmurais;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

public class Main {

    public static void main(String[] args) {

        Login login = new Login();

        while(true) {
            login.login();
        }
    }
}
