package org.academiadecodigo.shellmurais;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class UsersVerification {

    private String name;
    private int password;

    public static Map<String, Integer> list = new HashMap<>();


    public UsersVerification(String name, int password){
        this.name = name;
        this.password = password;
    }

    public String getName(){
        return this.name;
    }

    public int getPassword() {
        return password;
    }

    public void newUser(){

        list.put(this.name, this.password);
    }
    public boolean isUser(String name, int password){

            if (list.containsKey(name)){
                if (pass(name, password)){
                    System.out.println("CORRECT LOGIN");

                    return true;
                }
            }

        System.out.println("USER NOT FOUND OR WRONG PASSWORD");
        return false;

    }
    public boolean pass(String name, int password){

        if(list.get(name).equals(password)){
            return true;
        }

        return false;
    }

}
