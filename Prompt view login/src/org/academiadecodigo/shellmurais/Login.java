package org.academiadecodigo.shellmurais;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.integer.IntegerInputScanner;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

public class Login {

    public static void login(){
        Prompt prompt = new Prompt(System.in, System.out);

        String[] options = {"Yes", "No"};
        MenuInputScanner scanner = new MenuInputScanner(options);

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("WELCOME TO OUR PAGE");
        System.out.println("--------------------------------------------------------------");
        System.out.println("Insert your Login Credentials bellow");

        StringInputScanner name = new StringInputScanner();
        name.setMessage("user name: \n");

        IntegerInputScanner password = new IntegerInputScanner();
        password.setMessage("pass: \n");

        String showName = prompt.getUserInput(name);
        int showPassword = prompt.getUserInput(password);

        UsersVerification usersVerification = new UsersVerification(showName,showPassword);
        if(usersVerification.isUser(showName, showPassword)){

                System.out.println("*******************************************");
                System.out.println("Login Sucessful");
                System.out.println("Welcome " + showName +
                        " / Password: " + password.hashCode());

        }else {
            scanner.setMessage("Do you want create a new account?");
            int answerIndex = prompt.getUserInput(scanner);
            System.out.println(options[answerIndex - 1]);
            if(answerIndex==1) {
                System.out.println("Welcome new user, we are Creating your account to login");
                usersVerification.newUser();
            }
            else{
                System.out.println("Try Again to login");
            }
        }
        System.out.println("-----------------------------------------------------------");


    }
}
