public class Room {
    private int nrRoom;
    private boolean occupied = false;
    private boolean key = true;

    public Room (int nrRooms, boolean occupied, boolean key){
        this.nrRoom = nrRooms;
        this.occupied = occupied;
        this.key = key;
    }

    public int getNrRoom() {
        return this.nrRoom;
    }
    public boolean getOccupied(){
        return this.occupied;
    }

    public boolean isOccupied() {
        if (occupied == false) {
            System.out.println("Your Room is " + nrRoom + " Enjoy!!");
            isKey();
            occupied = true;
            return occupied;
        }
        else {
            System.out.println("Room " + nrRoom + " It's occupied!!");
            occupied = false;
            return occupied;
        }
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }

    public boolean isKey() {
        if(key == true){
            System.out.println("Take the keys of Room " + nrRoom);
            key = false;
            return key;
        }
        else{
            System.out.println("Thanks for your visit!!! ");
            key = true;
            return key;
        }

    }
}
