public class Main {

    public static void main(String[] args) {
        String hotelName = "Sherington";
        String clientName = "Rodrigo";
        String client2 = "jose";
        String client3 = "Ana";
        int nrRooms = 1;
        Hotel hotel = new Hotel(hotelName, nrRooms);
        Person p1 = new Person(clientName, hotel, nrRooms);
        Person p2 = new Person(client2, hotel, nrRooms);
        Person p3 = new Person(client3, hotel, nrRooms);

        p1.checkIn();
        System.out.println("--------------");
        p2.checkIn();
        System.out.println("---------------");
        p1.checkOut();
        System.out.println("----------------");
        p1.checkIn();
        System.out.println("*****************");

        p3.checkIn();
        System.out.println("--------------");
        //p2.checkIn();
        //System.out.println("---------------");
        p2.checkOut();
        System.out.println("----------------");
        p2.checkIn();
    }
}
