public class Person {
    private String name;
    private Hotel hotel;
    private int nrRooms;
    private boolean key;

    public Person(String name, Hotel hotel, int nrRooms){
        this.name = name;
        this.hotel = hotel;
        this.nrRooms = nrRooms;
    }

    public String getName(){
        return this.name;
    }
    public int getNrRooms(){
        return this.nrRooms;
    }

    public void checkIn(){
        System.out.println(name);
        hotel.checkIn(nrRooms);

    }
    public void checkOut(){
        hotel.checkOut();
    }


}
