package org.academiadecodigo.shellmurais;

import org.academiadecodigo.shellmurais.animals.Animals;
import org.academiadecodigo.shellmurais.animals.Pig;

public class Program {
    public static void main(String[] args) {


        AnimalFarm animalFarm = new AnimalFarm(100);
        Animals[] animals = new Animals[animalFarm.getQtdAnimals()];

        for(int i=0; i< animalFarm.getQtdAnimals(); i++) {
            animals[i] = animalFarm.sellAnimal();
            animals[i].speak();
            animals[i].animalEspecie();
            System.out.println("****************");
            if(animals[i] instanceof Pig){
                animalFarm.pig();
                return;
            }
        }

    }
}
