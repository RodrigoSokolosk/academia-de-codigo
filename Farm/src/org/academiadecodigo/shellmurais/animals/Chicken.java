package org.academiadecodigo.shellmurais.animals;

public class Chicken extends Animals{
    private String especie = this.getClass().getSimpleName();
    private String language = "Popopo";

    public Chicken(){
        super();
        //this.especie = "Chicken";
    }
    @Override
    public void speak(){
        System.out.println(language);
    }

    @Override
    public void animalEspecie(){
        System.out.println(especie);

    }
}
