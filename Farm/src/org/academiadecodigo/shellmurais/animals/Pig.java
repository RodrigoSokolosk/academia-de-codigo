package org.academiadecodigo.shellmurais.animals;

public class Pig extends Animals{

    private String especie= this.getClass().getSimpleName();
    private String language = "Oinc";
    public Pig(){
        super();
        //this.especie = "Pig";
    }
    @Override
    public void speak(){
        System.out.println(language);
    }

    @Override
    public void animalEspecie(){
        System.out.println(especie);

    }

}
