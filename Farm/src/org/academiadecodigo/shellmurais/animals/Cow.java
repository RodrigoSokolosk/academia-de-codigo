package org.academiadecodigo.shellmurais.animals;

public class Cow extends Animals{
    private String especie = this.getClass().getSimpleName();
    private String language = "Muuuuu";

    public Cow(){
        super();
        //this.especie = "Chicken";
    }
    @Override
    public void speak(){
        System.out.println(language);
    }

    @Override
    public void animalEspecie(){
        System.out.println(especie);

    }
}
