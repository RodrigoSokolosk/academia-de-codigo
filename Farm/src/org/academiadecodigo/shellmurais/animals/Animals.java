package org.academiadecodigo.shellmurais.animals;

public abstract class Animals {

    private String especie;
    private String language;

    public abstract void speak();
    public abstract void animalEspecie();

}
