package org.academiadecodigo.shellmurais;

import org.academiadecodigo.shellmurais.animals.Animals;
import org.academiadecodigo.shellmurais.animals.Chicken;
import org.academiadecodigo.shellmurais.animals.Cow;
import org.academiadecodigo.shellmurais.animals.Pig;

public class AnimalFarm {
    private int qtdAnimals;
    //private Animals[] animals;
    Pig pig;
    Cow cow;
    Chicken chicken;
    private boolean buyPig = false;

    public AnimalFarm(int qtdAnimals){
        this.qtdAnimals = qtdAnimals;
        //this.animals = new Animals[qtdAnimals];
    }
    public int getQtdAnimals(){
        return this.qtdAnimals;
    }

    public Animals sellAnimal() {
            int animalProb = (int) Math.abs(Math.random() * qtdAnimals);
            if (animalProb <= (0.05 * qtdAnimals)) {
                return pig = new Pig();
            }
            else if (animalProb <= (0.45 * qtdAnimals)) {
                return cow = new Cow();
            }
            else {
                 return chicken = new Chicken();
            }

    }
    public boolean pig(){
        System.out.println("Buy a Pig!!!");
        return buyPig = true;

    }
}
