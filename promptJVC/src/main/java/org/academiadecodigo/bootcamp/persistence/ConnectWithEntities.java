package org.academiadecodigo.bootcamp.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class ConnectWithEntities {

    private EntityManagerFactory emf;
    private EntityManager em;

    public ConnectWithEntities(){
        init();
    }

    public void init(){
         emf = Persistence.createEntityManagerFactory("test");
         em = emf.createEntityManager();

        System.out.println("Test Connection: " + em.createNativeQuery("SELECT 1+1").getSingleResult() );
    }

    public EntityManagerFactory getEmf() {
        return emf;
    }

    public EntityManager getEm() {
        return em;
    }
}
