package org.academiadecodigo.bootcamp.service;

import org.academiadecodigo.bootcamp.model.User;
import org.academiadecodigo.bootcamp.persistence.ConnectWithEntities;

import java.util.List;

public class UserServiceJPA implements UserService{


    private ConnectWithEntities connectWithEntities;
    public UserServiceJPA(ConnectWithEntities connectWithEntities){
        this.connectWithEntities = connectWithEntities;
    }
    @Override
    public boolean authenticate(String username, String password) {
        return false;
    }

    @Override
    public void add(String username, String email, String password, String firstname, String lastname, String phone) {
        User user = new User();

        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(password);
        user.setFirstName(firstname);
        user.setLastName(lastname);
        user.setPhone(phone);

        connectWithEntities.getEm().getTransaction().begin();
        connectWithEntities.getEm().persist(user);

        connectWithEntities.getEm().getTransaction().commit();

    }

    @Override
    public User findByName(String username) {
        return null;
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public int count() {
        return 0;
    }

}
