package org.academiadecodigo.bootcamp.service;

import org.academiadecodigo.bootcamp.model.User;
import org.academiadecodigo.bootcamp.persistence.ConnectionManager;
import org.academiadecodigo.bootcamp.utils.Security;

import java.sql.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class JdbcUserService implements UserService{

        private Connection dbConnection;
            // create a new statement

        public JdbcUserService(Connection dbConnection) {
            this.dbConnection = dbConnection;

        }


        @Override
        public boolean authenticate(String username, String password) {

           boolean result = false;
            try {

                String query = "SELECT * FROM user WHERE username=? AND password=?";

                PreparedStatement statement = dbConnection.prepareStatement(query);

                statement.setString(1, username);
                statement.setString(2, Security.getHash(password));

                ResultSet nameResultSet = statement.executeQuery();

                if (nameResultSet.next()) {
                    result = true;
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            return result;

        }

    @Override
    public void add(String username, String email, String password, String firstname, String lastname, String phone) {

    }

//    @Override
//        public void add(User user) {
//
//            try {
//
//                String query = "INSERT INTO user(username, email, password, firstname, lastname, phone) VALUES(?,?,?,?,?,?)";
//
////                + "values("'dfdf'")"
//
//                PreparedStatement statement = dbConnection.prepareStatement(query);
//
//                statement.setString(1, user.getUsername() );
//                statement.setString(2, user.getEmail());
//                statement.setString(3, user.getPassword());
//                statement.setString(4, user.getFirstName());
//                statement.setString(5, user.getLastName());
//                statement.setString(6, user.getPhone());
//
//                statement.executeUpdate();
//
//            } catch (SQLException throwables) {
//                throwables.printStackTrace();
//            }
//
//        }

        @Override
        public User findByName(String username) {

        try {

                String query = "SELECT * FROM user WHERE username =?";
                PreparedStatement statement = dbConnection.prepareStatement(query);

                statement.setString(1, username);
                ResultSet resultSet = statement.executeQuery();

                if (resultSet.next()) {

//                    return new User(resultSet.getString("username"), resultSet.getString("email"),
//                            resultSet.getString("password"), resultSet.getString("firstname"),
//                            resultSet.getString("lastname"), resultSet.getString("phone"));
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            return null;
        }

        @Override
        public List<User> findAll() {

            List<User> users = new LinkedList<>();
            try {

                String query = "SELECT * FROM user";
                PreparedStatement statement = dbConnection.prepareStatement(query);

                ResultSet resultSet = statement.executeQuery();
                while (resultSet.next()) {

//                   users.add(new User(resultSet.getString("username"), resultSet.getString("email"),
//                            resultSet.getString("password"), resultSet.getString("firstname"),
//                            resultSet.getString("lastname"), resultSet.getString("phone")));
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            return users;
        }

        @Override
        public int count() {

            int result = 0;
            try {

            String query = "SELECT COUNT(*) FROM user";

            PreparedStatement statement = dbConnection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                    result = resultSet.getInt(1);
                }

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

            return result;
        }

}
