package org.academiadecodigo.shellmurais.files;

import org.academiadecodigo.shellmurais.exceptions.FileException;
import org.academiadecodigo.shellmurais.exceptions.FileNotFoundException;
import org.academiadecodigo.shellmurais.exceptions.NotEnoughPermissionException;
import org.academiadecodigo.shellmurais.exceptions.NotEnoughSpaceException;

public class FileManager {
    private boolean isLogged;
    private File[] files;

    public FileManager(){
        this.isLogged = false;
        files = new File[5];
    }

    public void login(){
        isLogged = true;
        System.out.println("Login");
    }
    public void logout(){
        isLogged = false;
        System.out.println("Logout");
    }


    public File getFile(String file) throws FileNotFoundException {

        for (File allFile : files) {

            if (allFile == null) {
                break;
            }

            if (allFile.getName().equals(file)) {
                return allFile;
            }
        }
        throw new FileNotFoundException();
    }

    public void createFile(String newFile) throws NotEnoughSpaceException, NotEnoughPermissionException {
        if (!isLogged) {
            throw new NotEnoughPermissionsException();
        }

        if (fileCounter == files.length) {
            throw new NotEnoughSpaceException();
        }

        files[fileCounter] = new File(filename);
        fileCounter++;

    }
}

























/* public class FileManager {

    private File[] files;
    private boolean loggedIn = false;
    private int fileCounter = 0; // avoids countinously iterating over the files array

    public FileManager(int maxFiles) {
        this.files = new File[maxFiles];
    }

    public void login() {
        loggedIn = true;
    }

    public void logout() {
        loggedIn = false;
    }

    public File getFile(String filename) throws FileNotFoundException {

        for (File file : files) {

            if (file == null) {
                break;
            }

            if (file.getName().equals(filename)) {
                return file;
            }
        }

        throw new FileNotFoundException();
    }

    public void createFile(String filename) throws NotEnoughPermissionsException, NotEnoughSpaceException {

        if (!loggedIn) {
            throw new NotEnoughPermissionsException();
        }

        if (fileCounter == files.length) {
            throw new NotEnoughSpaceException();
        }

        files[fileCounter] = new File(filename);
        fileCounter++;

    }

}
 */