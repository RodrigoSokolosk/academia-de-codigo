package org.academiadecodigo.shellmurais.exceptions;

public class NotEnoughSpaceException extends FileException{

    private String message;

    @Override
    public String msg(){
        return message = "Not Enough Space";
    }
}
