package org.academiadecodigo.shellmurais;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Unique implements Iterable<String>{

    private Set<String> set;
    private String string;

    public Unique(String string){
        set = new HashSet<>();
        this.string = string;
        add(string);
    }

    public Iterator<String> iterator(){
        return set.iterator();
    }
    public void add(String str){
        String[] strArray = str.split(" ");
        for(String word : strArray){
        set.add(word);
        }

    }

}
