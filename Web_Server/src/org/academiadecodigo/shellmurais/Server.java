package org.academiadecodigo.shellmurais;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
import java.util.TimeZone;

public class Server {

    public static void main(String[] args) {


        try {
            Scanner scanner = new Scanner(System.in);
            ServerSocket serverSocket = new ServerSocket(5050);
            System.out.println("waiting for connection");
            Socket clientSocket = serverSocket.accept();
            if(clientSocket.isConnected()){
                System.out.println("Computer " + clientSocket.getInetAddress() + " are connected.");
            }
            System.out.println("Waiting message...");

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            //PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            String message = in.readLine();

            String[] dataReq = message.split(" ");
            String way = dataReq[0];
            String path = dataReq[1];
            String protocol = dataReq[2];
            System.out.println("*****" + way);
            System.out.println("++++++" + protocol);
            System.out.println("------" + path);

            while (!message.isEmpty()) {
                System.out.println(message);
                message = in.readLine();
            }
            DataOutputStream outFile = null;
            if (path.equals("/")) {
                path = "index.html";
                outFile = new DataOutputStream(clientSocket.getOutputStream());
                System.out.println(path);
            }
            FileInputStream file = new FileInputStream("resources/Rodrigo.jpeg");
            //FileOutputStream outFile = new FileOutputStream(path);
            byte[] buffer = new byte[1024];
            int byteread;
            while((byteread = file.read(buffer)) !=-1){
              outFile.write(buffer, 0, byteread);
            }
            String status = protocol + " 200 Document Folows\r\n";
            /*if (path.isEmpty()) {
                status = protocol + " 404 Not Found\r\n";
                file = new File("404.html");
            }*/
            System.out.println(status);

            SimpleDateFormat format = new SimpleDateFormat("E, dd MMM yyyy hh:mm:ss", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone("GMT"));
            Date data = new Date();
            String formatedData = format.format(data) + " GMT";

            String header = status
                    + "Location: https://localhost:5050/\r\n"
                    + "Date: " + formatedData + "\r\n"
                    + "Server: MyServer/1.0\r\n"
                    + "Content-Type: text/html\r\n"
                    //+ "Content-Length: " + contend.length + "\r\n"
                    + "Connection: close\r\n"
                    + "\r\n";

            OutputStream answer = clientSocket.getOutputStream();

            answer.write(header.getBytes());
            System.out.println(header);

            answer.write(Integer.parseInt(header));

            answer.flush();


            //while (/*!message.isEmpty()*/true) {
              //  System.out.println(message);
                //message = in.readLine();
            //}
               /* System.out.println("Server write your message: ");
                String newMessage = scanner.nextLine();
                out.println(newMessage);*/
                //}
            }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
}
