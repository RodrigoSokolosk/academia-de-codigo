package org.academiadecodigo.shellmurais;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {

            try {

               Socket clientSocket = new Socket(InetAddress.getLocalHost(), 5050);
               if(clientSocket.isConnected()){
                   System.out.println("Connected to: " + clientSocket.getInetAddress());
               }
               else{
                   System.out.println("Disconected");
               }
                String requisicao = ""
                        + "GET / HTTP/1.1\r\n"
                        + "Host: www.google.com.br\r\n"
                        + "\r\n";

                OutputStream sendMsg = clientSocket.getOutputStream();
                byte[] buffer = requisicao.getBytes();
                sendMsg.write(buffer);
                sendMsg.flush();

                Scanner sc = new Scanner(clientSocket.getInputStream());

                while (sc.hasNext()) {

                    System.out.println(sc.nextLine());
                }







            } catch (IOException e) {
                e.printStackTrace();
            }

    }
}
