public class Person {
    private String name;
    private Hotel hotel;
    public int idRoom = hotel.noRoom;

    public Person(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
    public void setHotel(Hotel hotel){

        this.hotel = hotel;
    }
    public String getHotel(){
        return hotel.getHotelName();
    }

    public boolean checkIn(){
        if(hotel == null || idRoom != hotel.noRoom ){
            System.out.println("get hotel first!");
            return false;
        }
        idRoom = hotel.checkIn();

        if(idRoom == hotel.noRoom){
            return false;
        }
        return true;
    }
}
