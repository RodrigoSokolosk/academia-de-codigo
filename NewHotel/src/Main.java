public class Main {

    public static void main(String[] args) {
        Person person1 = new Person("Rodrigo");
        Person person2 = new Person("Ana");

        Hotel hotel = new Hotel("Ritz", 1);
        Hotel hotel2 = new Hotel("Plaza", 1);

        person1.setHotel(hotel);
        person2.setHotel((hotel2));
        System.out.println(person2.getName());
        System.out.println(person2.getHotel());
    }

}
