public class Hotel {
    private String hotelName;
    private Room[] rooms;
    public static final int noRoom = -1;

    public Hotel(String hotelName, int nrRooms) {
        this.hotelName = hotelName;
        rooms = new Room[nrRooms];
    }

    public String getHotelName() {
        return this.hotelName;
    }

    public int checkIn() {
        for (int i = 0; i < rooms.length; i++) {
            if (rooms[i] == null) {
                rooms[i] = new Room();
            }
            if (rooms[i].isAvailable()) {
                rooms[i].occupy();
                return i;
            }
        }
        return noRoom;
    }
}



