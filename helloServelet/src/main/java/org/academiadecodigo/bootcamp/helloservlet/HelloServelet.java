package org.academiadecodigo.bootcamp.helloservlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HelloServelet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        User user = new User();
//        user.setName("Catarina Campino");
//        user.setEmail("catarina.campino@academiadecodigo.org");
//        req.setAttribute("user", user);
//        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/WEB-INF/index.jsp");
//        requestDispatcher.forward(req, resp);

        resp.sendRedirect("/hello/index.html");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("username");
        if (name==null || name.isEmpty()){
            resp.getWriter().printf("<h1>Wrong login data</h1>");
            resp.sendRedirect("/hello/index.html");
        }
        else{
            resp.getWriter().printf("<h1>Hello Mr. %s, Welcome</h1>", name);
        }
    }
}
