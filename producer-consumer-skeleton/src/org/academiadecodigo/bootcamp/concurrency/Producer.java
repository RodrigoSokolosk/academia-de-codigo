package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Integer> queue;
    private int elementNum;

    /**
     * @param queue the blocking queue to add elements to
     * @param elementNum the number of elements to produce
     */
    public Producer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public void run() {
        synchronized (queue) {
            while (elementNum > 0) {
                queue.offer(elementNum);
                System.out.println("Thread " + Thread.currentThread().getName() + " produce a element!!");
                elementNum--;
                if(queue.getSize() == queue.getLimit()){
                    System.out.println("Thread " + Thread.currentThread().getName() + " are full");
                }
            }
        }
    }
}
