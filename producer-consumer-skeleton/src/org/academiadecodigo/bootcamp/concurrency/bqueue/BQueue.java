package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Blocking Queue
 * @param <T> the type of elements stored by this queue
 */
public class BQueue<T> {

    private int limit;
    private Queue queue;
    private boolean canRemove = false;

    /**
     * Constructs a new queue with a maximum size
     * @param limit the queue size
     */
    public BQueue(int limit) {
        this.queue = new LinkedList();
        this.limit = limit;

    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     * @param data the data to add to the queue
     */
    public void offer(T data) {
        if (getSize() >= getLimit() ) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                notifyAll();
//                System.out.println(Thread.currentThread().getName() + " Produce: after WAIT");
            }

            queue.offer((Integer) data);
            System.out.println(Thread.currentThread().getName() + " Produce: ");
            notifyAll();

    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     * @return the data from the head of the queue
     */
    public T poll() {

        while (queue.isEmpty()){
                    try {
                        wait();
//                        System.out.println(Thread.currentThread().getName() + "Removed item After wait");
                        notifyAll();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
        }

                    queue.remove();
                    System.out.println(Thread.currentThread().getName() + "Removed item");
                    notifyAll();
                    return null;

    }

    /**
     * Gets the number of elements in the queue
     * @return the number of elements
     */
    public int getSize() {
        return queue.size();

    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     * @return the maximum number of elements
     */
    public int getLimit() {
        return limit - queue.size();

    }

}
