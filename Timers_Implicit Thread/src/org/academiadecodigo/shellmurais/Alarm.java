package org.academiadecodigo.shellmurais;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Alarm {

    Timer timer;
    Timer timer1;
    Timer timerAll;

    public static void main(String[] args) {

        System.out.println(Thread.currentThread().getName());
        Scanner scanner = new Scanner(System.in);

        System.out.println("nr rinsg:");
        int numRing = Integer.parseInt(scanner.next());

        System.out.println("ring interval:");
        int ringInterval = Integer.parseInt(scanner.next());

        Alarm alarm = new Alarm();
        alarm.start(ringInterval, numRing);
    }

    private void start(int interval, int num){

        timer = new Timer();
        timer.scheduleAtFixedRate(new Ring(num), interval*1000, interval*1000);

        timer1 = new Timer();
        timer1.scheduleAtFixedRate(new Ring(num), interval*1000, interval*1000);

        timerAll = new Timer();
        timerAll.scheduleAtFixedRate(new Ring(num), interval*1000, interval*1000);

    }

    public class Ring extends TimerTask{

        int numRings;

        public Ring(int numRings){
            this.numRings = numRings;
        }

        @Override
        public void run() {

            System.out.println(Thread.currentThread().getName());

            System.out.println("alarm...");
            numRings--;

            if(numRings==0){
                System.out.println("");
                stop();
            }

        }
        private void stop(){

            timer.cancel();
            timer1.cancel();
            timerAll.cancel();
        }


    }
}
