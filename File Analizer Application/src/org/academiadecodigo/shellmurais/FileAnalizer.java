package org.academiadecodigo.shellmurais;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileAnalizer {

    private Stream<String> file;

    public FileAnalizer(){

    }

    public void readFile(Path path) throws IOException {
          file = Files.lines(path);
          file.forEach(System.out::println);

    }

    public void countingWords(Path path) throws IOException {

        long wordCount = Files.lines(path).flatMap(line -> Stream.of(line.split("[^a-zA-Z0-9]")))
        .count();
        System.out.println(wordCount);
    }

    public void firstWord(Path path, int wordLenght) throws IOException {

        Optional<String> file = Files.lines(path).flatMap(line -> Stream.of(line.split(" ")))
                .filter(word -> word.length() == wordLenght).findFirst();
        if (file.isPresent()) {
            System.out.println("The first word with length " + wordLenght + " is " + file.get());
        } else {
            System.out.println("There no longer words");
        }

    }

    public void gettingNLongest(Path path, int words) throws IOException {

        String file = Files.lines(path).flatMap(line -> Stream.of(line.split(" ")))
                .sorted((a, b ) -> b.length() - a.length())
//                .sorted(Comparator.comparingInt(String::length).reversed())
                .distinct()
                .limit(words)
                .reduce("", (acc, element) -> acc + " " + element);

        System.out.println(file);
        System.out.println();
    }

    public void commonWord(Path path) throws IOException {

        List<String> listInFile1 = Files.lines(path).flatMap(line -> Stream.of(line.split("[^a-zA-Z0-9]"))).collect(Collectors.toList());
        List<String> commonWords = Files.lines(Paths.get("resources/sample2.txt")).
                flatMap(line -> Stream.of(line.split("[^a-zA-Z0-9]")))
                .filter(word -> listInFile1.contains(word)).collect(Collectors.toList());

        System.out.println("Common Words: " + commonWords);
    }

}
