package org.academiadecodigo.shellmurais;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class App {

    public static void main(String[] args) {

        FileAnalizer fileAnalizer = new FileAnalizer();
        Path path = Paths.get("resources/sample3.txt");

        try {
            fileAnalizer.readFile(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fileAnalizer.countingWords(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fileAnalizer.firstWord(path, 10);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fileAnalizer.gettingNLongest(path, 10);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            fileAnalizer.commonWord(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
