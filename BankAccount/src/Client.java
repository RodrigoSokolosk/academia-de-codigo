public class Client {

    private String name;
    private int account;
    private int wallet = 250;
    public Bank bank;
    public ExpendMoney expendMoney;

    public Client(String name, int account, Bank myBank){
        this.name = name;
        this.account = account;
        this.bank=myBank;

    }

    public String getName(){
        return this.name;
    }

    public int getAccount(){
        return this.account;
    }

    public int getWallet(){
        return this.wallet;
    }

    public int withdraw(int amount){

        wallet += bank.bankWithdraw(amount);
        return amount;
    }

    public int deposit(int amount){
        if(wallet<amount){
            System.out.println("Not Enough Money in your wallet to Deposit!!");
        return 0;
        }
        else {
            wallet -= bank.bankDeposit(amount);
            return amount;
        }
    }

    public int balance(){
        return bank.balance();
    }

    public int work(int hours, int payment) {
        int salary = hours * payment;
        bank.bankDeposit(salary);
        return salary;
    }

    public int expendFromWallet(){
        int item = ExpendMoney.buyStuffs().getPrice();
        String product = ExpendMoney.buyStuffs().getProduct();
        System.out.println("Buy " + product + " From wallet, This item costs " + item);
        if(item>=wallet){
            System.out.println("Not Enough Money in your wallet!!");
            return wallet;
        }
        else {
            return wallet -= item;
        }
    }

    public int expendFromAccount(){
        int item = ExpendMoney.buyStuffs().getPrice();
        String product = ExpendMoney.buyStuffs().getProduct();
        System.out.println("Buy " + product + " From Account, This item costs " + item);
        if(bank.balance()<=item){
            System.out.println("Not Enough Money in your account to buy " + product);
            return bank.balance();
        }
        else {
            return bank.bankWithdraw(item);
        }
    }
}
