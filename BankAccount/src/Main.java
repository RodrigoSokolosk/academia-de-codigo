public class Main {

    public static void main(String[] args) {

        Bank bankAccount = new Bank("Rodrigo",123);
        Client client = new Client(bankAccount.getName(), bankAccount.getAccountNumber(), bankAccount);
        System.out.println(bankAccount.getName() + ", account Number = " + bankAccount.getAccountNumber());

        /***Balance in account and wallet***/
        System.out.println("Account balance = " + client.balance());
        System.out.println("Money in wallet = " + client.getWallet());
        System.out.println();

        /***Work and balance***/
        System.out.println("You worked and receive " + client.work(5, 80) + " in your account!!");
        System.out.println("Account balance = " + client.balance());
        System.out.println("Money in wallet = " + client.getWallet());
        System.out.println();

        /***Deposit from wallet to account and balance***/
        client.deposit(100);
        System.out.println("Account balance = " + client.balance());
        System.out.println("Money in wallet= " + client.getWallet());
        System.out.println();

        /***Withdraw from account to wallet***/
        System.out.println("Withdraw= " + client.withdraw(100));
        System.out.println("Account balance = " + client.balance());
        System.out.println("Money in wallet = " + client.getWallet());
        System.out.println();

        /***Deposit from wallet to account***/
        System.out.println("Deposit= " + client.deposit(35));
        System.out.println("Account balance = " + client.balance());
        System.out.println("Money in wallet = " + client.getWallet());
        System.out.println();

        client.expendFromWallet();
        client.expendFromAccount();

        System.out.println("Account balance = " + client.balance());
        System.out.println("Money in wallet = " + client.getWallet());
        System.out.println();
    }
}
