public class Bank {

    private String name;
    private int accountNumber;
    private int clientMoney = 100;

    public Bank(String name, int accountNumber) {
        this.accountNumber = accountNumber;
        this.name = name;
    }

    public int getAccountNumber() {
        return this.accountNumber;
    }

    public String getName() {
        return this.name;
    }

    public int balance(){
        return this.clientMoney;
    }

    public int bankWithdraw(int clientMoney) {
        if (this.clientMoney < clientMoney) {
            System.out.println("Not enough Money to Withdraw!!");
            return 0;

        }
        else {
            this.clientMoney -= clientMoney;
            return clientMoney;
        }
    }

    public int bankDeposit(int clientMoney){
        this.clientMoney += clientMoney;
        return clientMoney;
    }


}
