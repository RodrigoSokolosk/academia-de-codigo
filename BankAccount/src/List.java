public enum List {
    Grocery("Grocery", 100),
    Drinks("Drinks", 200),
    Clothes("Clothes", 150),
    Shoes("Shoes", 180);


    private int price;
    private String product;

    List(String product, int price){
        this.product = product;
        this.price = price;
    }
    public int getPrice(){
        return this.price;
    }
    public String getProduct(){
        return this.product;
    }
}

