package org.academiadecodigo.shellmurais;

import java.util.HashMap;
import java.util.Map;

public class User {

    private String name;
    private int id;
    private String userName;
    private int password;
    public static Map<String, Integer> list = new HashMap<>();

    public User(String name, int id, String userName, int password){
        this.name = name;
        this.id = id;
        this.userName = userName;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public int getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void newUser(){

        list.put(this.name, this.password);
    }


    public boolean isUser(String name, int password){

            if (list.containsKey(name)){
                if (pass(name, password)){
                    System.out.println("CORRECT LOGIN");

                    return true;
                }
            }

            System.out.println("USER NOT FOUND OR WRONG PASSWORD");
            return false;

        }
        public boolean pass(String name, int password){

            if(list.get(name).equals(password)){
                return true;
            }

            return false;
        }
}
