package org.academiadecodigo.shellmurais;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    public final static int PORTNUMBER = 8080;
    private static ExecutorService cachedPool;
    private static Socket clientSocket;
    private static CopyOnWriteArrayList<Task> taskArray = new CopyOnWriteArrayList<>();

    public static void main(String[] args) {

        try {
            ServerSocket serverSocket = new ServerSocket(PORTNUMBER);
            System.out.println("Waiting for connection");
            cachedPool = Executors.newCachedThreadPool();

            while(true){
                clientSocket = serverSocket.accept();
                Task task = new Task(clientSocket);
                taskArray.add(task);

                cachedPool.submit(task);
//                Thread thread = new Thread(new Task());
//                thread.start();

            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        }

        public static class Task implements Runnable{

        private Socket socket;
        private User user;

        public Task(Socket socket){
            this.socket = socket;

        }

            @Override
            public void run() {

                this.user = login(socket);

                BufferedReader in = null;
                try {
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                while (true) {

                    try {
                        String message = in.readLine();

                        if (message == null || message.equals("/quit")) {
                            System.out.println("Client " + Thread.currentThread().getName() + " closed");
                            break;
                        }

                        System.out.println(message);
                        broadcast(message, socket, user.getName());


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        public static User login(Socket socket){

            try {
                PrintWriter pw = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader bR = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                pw.println("User Name: ");
                String userName = bR.readLine();
                pw.println("Password: ");
                int password = bR.read();
                System.out.println("user name: " + userName +
                    " password: " + password);

                User user = new User(userName, taskArray.size(), userName, password);
                user.isUser(userName, password);///****implement login continue here
                return user;

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
    }

        public static void broadcast(String message, Socket clientSocket, String name){

        for(Task task: taskArray){
            PrintWriter out = null;
            try {

                if (task.socket.equals(clientSocket)){
                    continue;
                }
                out = new PrintWriter(task.socket.getOutputStream(), true);
                out.println("Write your message");
                System.out.println(task);
                out.println( name + ": " + message);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        }

    }
