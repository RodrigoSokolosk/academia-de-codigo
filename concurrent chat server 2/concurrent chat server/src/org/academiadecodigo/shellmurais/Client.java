package org.academiadecodigo.shellmurais;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Client {

    private static Socket clientSocket;
    private static ExecutorService fixedPool = Executors.newFixedThreadPool(2);
    private String name;


    public static void main(String[] args) {

        Client client = new Client();

        try {

            //Scanner scanner = new Scanner(System.in);
            //client.name = scanner.next();


            clientSocket = new Socket(InetAddress.getLocalHost(), Server.PORTNUMBER);
            System.out.println("Connected to Server");

            fixedPool.submit(new WriteMessage());
//            Thread thread1 = new Thread(new WriteMessage());
//            thread1.start();

            fixedPool.submit(new receiveMessage());
//            Thread thread2 = new Thread(new receiveMessage());
//            thread2.start();

        } catch (IOException e) {
            e.printStackTrace();
        }

        //System.out.println(client.name);

    }

    public String getName() {
        return name;
    }

    public static class WriteMessage implements Runnable {

        private BufferedReader inputBufferedReader;
        private BufferedWriter outputBufferedWriter;
        private static String line = "";

        @Override
        public void run() {
            try {
                inputBufferedReader = new BufferedReader(new InputStreamReader(System.in));
                outputBufferedWriter = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }


            while (!line.equals("quit")) {
                try {
                    line = inputBufferedReader.readLine();
                    outputBufferedWriter.write(line);
                    outputBufferedWriter.newLine();
                    outputBufferedWriter.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                clientSocket.close();
                System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static class receiveMessage implements Runnable {

        BufferedReader input;

        {
            try {
                input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {

            while (true) {
                try {
                    String message = input.readLine();
                    if(message == null){
                        System.out.println("Closed by Server side");
                        System.exit(0);
                    }

                    System.out.println(message);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
