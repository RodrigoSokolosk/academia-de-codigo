package org.academiadecodigo.shellmurais;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
        String playerName = "Rodrigo";
        Start start = new Start();

        start.startGame(playerName);

    }

}
