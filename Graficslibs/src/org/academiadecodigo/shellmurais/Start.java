package org.academiadecodigo.shellmurais;

import org.academiadecodigo.shellmurais.game_objects.Asteroids;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Start {
    private Rectangle rect;
    private Picture backGround;
    private Player player;
    private Asteroids asteroids;
    public Start(){

    }
    public void startGame(String name){
        backGround();
        this.player = new Player(name);
        this.asteroids = new Asteroids();
        newPlayer();
        newAsteroid();


    }

    public void newPlayer(){
        player.newPlayer();
    }
    public void backGround(){
        backGround = new Picture(0, 0, "Gameplay BG.png");
        init();
    }
    public void newAsteroid(){
        asteroids.newObject();
    }
    public void init(){
        backGround.draw();

    }
}
