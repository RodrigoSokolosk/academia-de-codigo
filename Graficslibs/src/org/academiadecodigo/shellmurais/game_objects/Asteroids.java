package org.academiadecodigo.shellmurais.game_objects;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Asteroids {
    private int health;
    private Picture object;
    private int posx;
    private int posY;
    private int maxX;
    private int maxY;

    public Asteroids(){
        this.health = 10;
        maxX = 1000;
        maxY = -250;
        position();
        newObject();
    }

    public  void newObject(){
        this.object = new Picture(posx, posY,"smallAsteroid.png");

        init();
        object.grow(-300, -300);
    }
    public void init(){
        object.draw();
    }
    public void position(){
        posx = (int)(Math.random() * maxX);
        posY = (int)(Math.random() * maxY);
    }


}
