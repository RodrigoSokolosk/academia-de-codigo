package org.academiadecodigo.bootcamp.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

    private Connection connection;

    public ConnectionManager() {
        this.connection = null;
    }

    public Connection getConnection() {

        if (connection == null){
            try {
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ac?serverTimezone=UTC", "root", "");
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return connection;
    }

    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            System.out.println("Failure to close database connections: " + ex.getMessage());
        }
    }
}
