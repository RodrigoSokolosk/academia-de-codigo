package org.academiadecodigo.shellmurais.renting;

import org.academiadecodigo.shellmurais.singletable.Movies;

import javax.persistence.*;

@Entity(name = "RentingMovies")
@Table(name = "rentingtable")
public class RentMovie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(mappedBy = "rentMovie")

    private Client client;
    @OneToOne(mappedBy = "rentMovie")
    private Movies movies;

    public Integer getId() {
        return id;
    }

    public Client getClient() {
        return client;
    }

    public Movies getMovies() {
        return movies;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setMovies(Movies movies) {
        this.movies = movies;
    }
}
