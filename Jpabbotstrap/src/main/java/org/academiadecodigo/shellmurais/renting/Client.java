package org.academiadecodigo.shellmurais.renting;

import javax.persistence.*;

@Entity(name = "ClientTable")
@Table(name = "clienttable")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @OneToOne
    private RentMovie rentMovie;

    public void setRentMovie(RentMovie rentMovie) {
        this.rentMovie = rentMovie;
    }
}
