package org.academiadecodigo.shellmurais.singletable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name="ComedySingleTable")
@DiscriminatorValue("comedy")
public class ComedyMovies extends Movies{

    private String laughingscenes;

    public String getLaughingscenes() {
        return laughingscenes;
    }

    public void setLaughingscenes(String laughingscenes) {
        this.laughingscenes = laughingscenes;
    }
}
