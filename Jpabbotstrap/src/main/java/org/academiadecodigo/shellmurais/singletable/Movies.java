package org.academiadecodigo.shellmurais.singletable;

import org.academiadecodigo.shellmurais.renting.RentMovie;

import javax.persistence.*;

@Entity(name = "MoviesSingleTable")
@Table(name = "moviestable")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "movie_type",
        discriminatorType = DiscriminatorType.STRING
)
public abstract class Movies {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String director;
    private Integer rating;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDirector() {
        return director;
    }

    public Integer getRating() {
        return rating;
    }

    @OneToOne
    private RentMovie rentMovie;
}
