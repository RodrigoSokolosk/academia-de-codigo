package org.academiadecodigo.shellmurais.singletable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name="ActionSingleTable")
@DiscriminatorValue("action")
public class ActionMovies extends Movies {

    private String fightScenes;

    public String getFightScenes() {
        return fightScenes;
    }

    public void setFightScenes(String fightScenes) {
        this.fightScenes = fightScenes;
    }
}
