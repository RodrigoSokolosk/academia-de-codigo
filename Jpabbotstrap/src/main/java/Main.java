import org.academiadecodigo.shellmurais.singletable.ActionMovies;
import org.academiadecodigo.shellmurais.singletable.ComedyMovies;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("test");
        EntityManager em = emf.createEntityManager();
        EntityManager em2 = emf.createEntityManager();

        System.out.println("Result: " + em.createNativeQuery("SELECT 1+1").getSingleResult());


//        Film theShinning = new Film();
//        //transien
//        theShinning.setDirector("S. Kubrick");
//        theShinning.setTitle("The Shinning");
//        theShinning.setRating(10);
//        theShinning.setId(1);

        ActionMovies actionMovies = new ActionMovies();
        ComedyMovies comedyMovies = new ComedyMovies();

        actionMovies.setTitle("teste1");
        actionMovies.setDirector("eu");
        actionMovies.setRating(7);
        actionMovies.setFightScenes("fighting");

        comedyMovies.setTitle("teste2");
        comedyMovies.setDirector("outroeu");
        comedyMovies.setRating(5);
        comedyMovies.setLaughingscenes("kkkk");

        em.getTransaction().begin();
        em.persist(actionMovies);
        //persisted
        em.getTransaction().commit();

        em2.getTransaction().begin();
        em2.persist(comedyMovies);

        em2.getTransaction().commit();

        //detached
    }
}
