package org.academiadecodigo.shellmurais;

import java.util.Iterator;

public class Container<T> implements Iterable{
    protected int min;
    protected int max;
    protected boolean condition;
    private int counter;

    public Container(int min, int max, boolean condition){
        this.min = min;
        this.max = max;
        this.condition = condition;
        if(condition==false){
        counter = max+1;}
        else{counter=min-1;}

    }


    @Override
    public Iterator iterator() {

        if(condition){return new Iterator() {
            @Override
            public boolean hasNext() {
                if(counter<max){
                    return true;
                }
                return false;
            }

            @Override
            public Object next() {
                if(counter<max){
                    counter++;
                    return counter;
                }
                return null;
            }
        };

        }
        return new Iterator() {

            @Override
            public boolean hasNext() {
                if (counter > min) {
                    return true;
                }
                return false;
            }

            @Override
            public Object next() {
                if (counter > min) {
                    counter--;
                    return counter;
                }
                return null;
            }
        };

        }



}
