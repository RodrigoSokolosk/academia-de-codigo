package org.academiadecodigo.shellmurais.Game_Objects;

public enum BarrelType {
    PLASTIC(1, "Plastic Barrel"),
    WOOD(3, "Wood Barrel"),
    METAL(10, "Metal Barrel");

    public int maxDamage;
    public String message;

     BarrelType(int maxDamage, String message) {
        this.maxDamage = maxDamage;
        this.message = message;
    }

    public int getMaxDamage() {
        return maxDamage;
    }
    public String getMessage(){
         return message;
    }
    }