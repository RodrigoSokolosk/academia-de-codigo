package org.academiadecodigo.shellmurais.Game_Objects;

public class ArmouredEnemy extends Enemy{
    private int armour;
    private int health;
    private String name;

    public ArmouredEnemy(){
        this.armour = 10;
        this.health = 10;
        this.name = "Armoured Enemy";
    }
    public int getArmour(){
        return this.armour;
    }
    @Override
    public String getName(){
        return this.name;
    }

    @Override
    public int getHealth(){
        return this.health;
    }
    @Override
    public void hit(int damage){
        //int leftDamage = damage - armour;
        //if(leftDamage<0){leftDamage=0;}
        if(armour >= 0) {
            armour -= damage;
            if(armour<0){armour=0;}
            System.out.println("Shoot on armour!! Durability = " + getArmour());
        }
        if(armour <0){
            armour=0;
            health-= damage;
                System.out.println("Crashed Armor!!");
                System.out.println("Damage = " + damage);
            }

        if(health > 0 && armour <= 0) {
            health -= damage;
            if(health<=0){
                health=0;
            }
            //dead();

        }
        System.out.println("Enemy Life = " + getHealth());
        dead();
        //System.out.println("----------------------------------------------");

    }
    @Override
    public void dead(){
            if(health<=0) {
                System.out.println("Enemy Die!!!");
                super.dead();
            }
            else{
                System.out.println("Enemy Still Alive!!");
            }
            //System.out.println("+++++++++++++++++++++++++++++++++++++++++++");
    }
}
