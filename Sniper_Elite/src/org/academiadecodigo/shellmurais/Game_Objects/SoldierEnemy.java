package org.academiadecodigo.shellmurais.Game_Objects;

public class SoldierEnemy extends Enemy{
    private int health;
    private String name;

    public SoldierEnemy(){
        this.health = 10;
        this.name = "Soldier Enemy";
    }

    @Override
    public String getName(){
        return this.name;
    }

    @Override
    public int getHealth(){
        return this.health;
    }

    @Override
    public void hit(int damage){
        health -= damage;
        //System.out.println("");
        dead();
        System.out.println("health = " + getHealth());
    }
    @Override
    public void dead(){
        if(health <= 0) {
            health=0;
            super.dead();
            System.out.println("Enemy Die!!!");
           // System.out.println("+++++++++++++++++++++++++++++++++");
        }
        else{
            System.out.println("Enemy still alive!!!");
            //System.out.println("*********************************");
        }
    }

}
