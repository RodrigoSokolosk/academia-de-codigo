package org.academiadecodigo.shellmurais.Game_Objects;

public class Barrel extends GameObject implements Destroyable{
    private int currentDamage;
    private boolean destroyed;
    private String message;
    BarrelType barrelType;

    public Barrel(){

    this.destroyed = false;
    this.barrelType = setBarrelType();
    this.message = barrelType.getMessage();
    this.currentDamage = barrelType.maxDamage;

    }

    public BarrelType setBarrelType(){
        int type = (int)(Math.random() * (BarrelType.values().length));
        switch (type){
            case 0: barrelType = BarrelType.METAL;
            break;
            case 1: barrelType = BarrelType.WOOD;
            break;
            case 2: barrelType = BarrelType.PLASTIC;
            break;
        }
        return barrelType;
    }

    @Override
    public void hit(int damage){
        int health = currentDamage-=damage;
        System.out.println(currentDamage + " | " + health  );
        if(health<=0){
            getMessage();
            destroyBarrel();
        }
    }
    public void destroyBarrel(){
        destroyed = true;
    }

    @Override
    public boolean isDestroyed(){
        return destroyed;
    }
    @Override
    public String getMessage(){
        return this.message;
    }

}
