package org.academiadecodigo.shellmurais.Game_Objects;

public interface Destroyable {

    public void hit(int damage);
    public boolean isDestroyed();
}
