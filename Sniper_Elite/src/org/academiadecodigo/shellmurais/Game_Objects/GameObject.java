package org.academiadecodigo.shellmurais.Game_Objects;

import java.util.Random;

public abstract class GameObject {
    private GameObject[] gameObjects;
    private int pos;
    private String name;

    public String getName() {
        return this.name;
    }

    public abstract String getMessage();
}
