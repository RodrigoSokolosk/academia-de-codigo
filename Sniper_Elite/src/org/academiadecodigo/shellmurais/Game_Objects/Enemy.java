package org.academiadecodigo.shellmurais.Game_Objects;

public abstract class Enemy extends GameObject implements Destroyable{
    private String message;
    private int health;
    private boolean isDestroyed;
    private String name;

    public Enemy(){
        this.message = "Shooting Enemies!!!";
        this.isDestroyed = false;
        this.name = "Enemy";

    }

    public String getName(){
        return name;
    }
    public int getHealth(){
        return this.health;
    }
    @Override
    public abstract void hit(int damage);
        /*getMessage();
        if(health<=0){
            health=0;
        }
        System.out.println("Enemy health = " + getHealth());
        //System.out.println("++++++++++++++++++++++++++++++");
    }*/
    public void dead(){
        if(health <= 0) {
            health=0;
            //super.dead();
            //System.out.println("Enemy Die!!!");
            //System.out.println("+++++++++++++++++++++++++++++++++++++++++++");
        }
        else{
            System.out.println("Enemy still alive!!!");
            //System.out.println("*******************************************");
        }
        isDestroyed=true;
    }
    @Override
    public boolean isDestroyed(){
        return isDestroyed;
    }

    @Override
    public String getMessage(){
        return this.message;
    }
}
