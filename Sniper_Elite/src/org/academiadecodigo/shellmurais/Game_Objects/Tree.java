package org.academiadecodigo.shellmurais.Game_Objects;

public class Tree extends GameObject{
    private String message;
    private String name;

    public Tree(){
        this.message = "Can't shoot Tree!!";
        this.name = "Tree";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getMessage(){
        //System.out.println(message);
        return this.message;
    }
}
