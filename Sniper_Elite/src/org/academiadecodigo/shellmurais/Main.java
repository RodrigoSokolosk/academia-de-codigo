package org.academiadecodigo.shellmurais;

public class Main {
    public static void main(String[] args) {

        int nrOfObjects = 5;
        Game game = new Game(nrOfObjects);
        game.start();

    }
}
