package org.academiadecodigo.shellmurais;

import org.academiadecodigo.shellmurais.Game_Objects.Destroyable;

public class SniperRifle {
    private int bulletDamage;

    public SniperRifle(){
        this.bulletDamage = 1;
    }
    public int getBulletDamage(){
        int placeShoot = (int)(Math.random() * 6);
        switch (placeShoot){
            case 0:
            case 1:
            case 2:
                bulletDamage = 1;
                System.out.println("Arm shoot!!");
            break;
            case 3:
            case 4:
                bulletDamage = 3;
                System.out.println("Body shoot!!");
            break;
            case 5: bulletDamage = 10;
                System.out.println("Head shoot");
                break;
        }
        return this.bulletDamage;
    }

    public void shoot(Destroyable obj){
        obj.hit(getBulletDamage());
        System.out.println("shooting...");
        //getBulletDamage();
    }
}
