package org.academiadecodigo.shellmurais;

import org.academiadecodigo.shellmurais.Game_Objects.*;

public class Game {
    private GameObject[] gameObject;
    private SniperRifle sniperRifle;
    private int nrObj;
    private int shootsFired = 0;

    public Game(int nrObj) {
        this.nrObj = nrObj;
        sniperRifle = new SniperRifle();
        gameObject= new GameObject[nrObj];

    }

    public void start() {
        createObjects();

        for (int i = 0; i < gameObject.length; i++) {
            //System.out.println(gameObject[i].getName());
            if(gameObject[i] instanceof Tree ){
                gameObject[i].getMessage();
                System.out.println("************************");
            }
            if(gameObject[i] instanceof Destroyable) {
                while (((Destroyable) gameObject[i]).isDestroyed() !=true) {
                    sniperRifle.shoot((Destroyable) gameObject[i]);
                    this.shootsFired++;
                    //gameObject[i].getMessage();
                    System.out.println("***********************************");
                    System.out.println("");
                }
            }
        }
        System.out.println("You shoot " + shootsFired + " Times!!");
    }

    public GameObject createObjects() {
        for (int i = 0; i < nrObj; i++) {
            if (gameObject[i] == null) {
                int randomObj = (int) (Math.random() * 4);
                //System.out.println(randomObj);
                switch (randomObj) {
                    case 0:
                        gameObject[i] = new ArmouredEnemy();
                        System.out.println(gameObject[i].getMessage());
                        System.out.println("-------------------------");
                        break;
                    case 1:
                        gameObject[i] = new SoldierEnemy();
                        System.out.println(gameObject[i].getMessage());
                        System.out.println("--------------------------");
                        break;
                    case 2:
                        gameObject[i] = new Tree();
                        System.out.println(gameObject[i].getMessage());
                        System.out.println("-------------------------");
                        break;
                    case 3:
                        gameObject[i] = new Barrel();
                        System.out.println(gameObject[i].getMessage());
                        System.out.println("-------------------------");
               }
            }
        }
            return new Tree();
    }
}
