package org.academiadecodigo.shellmurais;

import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;

public class WordReader implements Iterable<String>{

    private LinkedList<String> words = new LinkedList<>();

    FileReader in = new FileReader("resources/text.txt");
    BufferedReader buffer = new BufferedReader(in);

    private String line = "";

    public WordReader() throws FileNotFoundException {
    }

    public boolean doIt() throws IOException {

        if (!words.isEmpty()) {
            words.clear();
        }

        if (((line = buffer.readLine()) != null)) {

            String[] wordArray = line.split(" ");

            for (String word : wordArray) {
                words.add(word);
            }
            return true;
        } else {
            buffer.close();
            return false;
        }
    }

    @Override
    public Iterator<String> iterator() {

        return words.iterator();
    }





/*
//    private String newPath = "resources.newText.txt";
//    private String[] newString;
    private FileReader fileReader;
    private BufferedReader bufferedReader;
    private String path;
//    private String line;

    public WordReader(String path) throws IOException {
        this.path = path;
        readFile(path);

    }

    public String readFile(String path) throws IOException {

        fileReader = new FileReader(path);
        bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();

//        String line = "";
//        String result = "";
////        newString = new String[];
//        String[] word;

//        while((line = bufferedReader.readLine()) !=null){
//            result += line + "\n";
//            splitLine(result);
//        }
        bufferedReader.close();
//        word = result.split(" ");
//       System.out.println(word);
//        writeFile(newPath, result);
    return line;
    }

    public String[] splitLine(String result){
        String[] word =  result.split(" ");

        //        for (String retval: word) {
//            System.out.println(retval);
////            return retval;
//        }
//        System.out.println("WORD " + word);
        return word;

    }


    *//*public String writeFile(String newPath, String text) throws IOException {
        FileWriter fileWriter = new FileWriter(newPath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(text);

        bufferedWriter.flush();
        bufferedWriter.close();
        System.out.println(bufferedWriter);
        return "";
    }*//*


    @Override
    public Iterator iterator() {

        return new Iterator() {
        @Override
        public boolean hasNext() {
            if(bufferedReader!=null){
                System.out.println("hasnext");
                return true;
            }
            return false;
        }

        @Override
        public String next() {
            int index = 0;
            String[] words;
            String newWord = "";
                while (hasNext() == true) {
            try {
                    words = (readFile(path).split(" "));
                    newWord = words[index];
                    index++;
                } catch(IOException e){
                    e.printStackTrace();
                }
            }
                return newWord;
        }

            @Override
            public void remove() {

            }
        };

    }*/
}
