package org.academiadecodigo.shellmurais;

import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ClientSide {

    public static int portNumber = 50000;
    //private ServerSide hostName;

    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        byte[] sendBuffer = scanner.nextLine().getBytes();
        //byte[] sendBuffer = "teste".getBytes();
        byte[] receiveBuffer = new byte[1024];


        DatagramSocket socket  = new DatagramSocket(portNumber);


           DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, InetAddress.getLocalHost(),ServerSide.portNumber);
           socket.send(sendPacket);

           DatagramPacket receivePacket = new DatagramPacket(receiveBuffer, receiveBuffer.length);
           socket.receive(receivePacket);

        System.out.println(new String(receivePacket.getData()));

        socket.close();
    }
}
