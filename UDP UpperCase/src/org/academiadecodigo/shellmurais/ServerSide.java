package org.academiadecodigo.shellmurais;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Locale;

public class ServerSide {

    public static int portNumber= 55000;
    public static String hostName;


    public static void main(String[] args) throws IOException {

        byte[] receivedBuffer = new byte[1024];
        //byte[] sendBuffer = new byte[1024];

        DatagramSocket socket = new DatagramSocket(portNumber);


        DatagramPacket datagramPacket = new DatagramPacket(receivedBuffer, receivedBuffer.length);
        socket.receive(datagramPacket);
        String receivedMsg = new String(datagramPacket.getData()).toUpperCase();
        System.out.println(receivedMsg);

        InetAddress address = datagramPacket.getAddress();
        int port = datagramPacket.getPort();

        DatagramPacket sendPacket = new DatagramPacket(receivedMsg.getBytes(), receivedMsg.getBytes().length, InetAddress.getLocalHost(), port);
        socket.send(sendPacket);


        socket.close();
    }
}
