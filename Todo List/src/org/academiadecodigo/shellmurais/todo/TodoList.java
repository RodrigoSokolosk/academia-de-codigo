package org.academiadecodigo.shellmurais.todo;

import java.util.PriorityQueue;

public class TodoList {

    private PriorityQueue<TodoItem> priorityQueue;

    public TodoList(){
        priorityQueue = new PriorityQueue<>();
    }

    public void add(TodoItem item){
        priorityQueue.offer(item);
    }

    public boolean isEmpty(){
        return priorityQueue.isEmpty();
    }

    public Object remove(){
        return priorityQueue.poll();

    }

}
