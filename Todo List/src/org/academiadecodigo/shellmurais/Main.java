package org.academiadecodigo.shellmurais;

import org.academiadecodigo.shellmurais.todo.Importance;
import org.academiadecodigo.shellmurais.todo.TodoItem;
import org.academiadecodigo.shellmurais.todo.TodoList;

public class Main {

    public static void main(String[] args) {

        TodoList todoList = new TodoList();

        todoList.add(new TodoItem(Importance.MEDIUM, 1, "Medium priority 1"));
        todoList.add(new TodoItem(Importance.LOW, 1, "Low priority 1"));
        todoList.add(new TodoItem(Importance.HIGH, 1, "High priority 1"));
        todoList.add(new TodoItem(Importance.LOW, 2, "Low priority 1"));
        todoList.add(new TodoItem(Importance.MEDIUM, 2, "Medium priority 1"));
        todoList.add(new TodoItem(Importance.HIGH, 2, "High priority 1"));
        System.out.println(todoList);
        while(!todoList.isEmpty()){
            System.out.println(todoList.remove());
        }
    }
}
