package org.academiadecodigo.shellmurais;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ClientSide {

    private static int portNumber;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        //portNumber = scanner.nextInt();
        //System.out.println(portNumber);



        try {
            Socket clientSocket = new Socket("127.0.0.1", 5050);

            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while(true) {

                System.out.println("Client Write your message:");
                String newMessage = scanner.nextLine();
                out.println(newMessage);
                System.out.println();

                String message = in.readLine();
                System.out.println(message);
            }

        } catch (IOException e) {
            System.out.println("can't connect");
        }


    }
}
