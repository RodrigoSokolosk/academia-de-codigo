package org.academiadecodigo.shellmurais;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class ServerSide {

    public static int portNumber;
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);
        portNumber = scanner.nextInt();

        ServerSocket serverSocket = new ServerSocket(portNumber);
        System.out.println("waiting for connection");
        Socket clientSocket = serverSocket.accept();
        System.out.println("Waiting message...");

        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        while (true) {
            String message = in.readLine();
            System.out.println(message);

            System.out.println("Server write your message: ");
            String newMessage = scanner.nextLine();
            out.println(newMessage);
        }

    }
}
