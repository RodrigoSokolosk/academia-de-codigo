package org.academiadecodigo.carcrash.cars;

public class Mustang {
    //private Car newCar;
    private String name;
    private int speed;
    private String symbol;

    public Mustang(String name, int speed, String symbol){
        super();
        this.name = name;
        this.speed = speed;
        this.symbol = symbol;

    }

    public static Car newM() {

        String carName = CarType.MUSTANG.getName();
        int speed = CarType.MUSTANG.getSpeed();
        String symbol = CarType.MUSTANG.getSymbol();
        System.out.println(carName + " " + speed + " "+ symbol);
        return new Fiat(carName, speed, symbol);

    /*@Override
    public String toString(){
        return this.symbol;
    }*/
    }
}
}
