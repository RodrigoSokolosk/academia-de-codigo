package org.academiadecodigo.carcrash.cars;

public class Fiat extends Car{

    private String name;
    private int speed;
    private String symbol;

    public Fiat(String name, int speed, String symbol){
        this.name = name;
        this.speed = speed;
        this.symbol = symbol;

    }

    public Car getNewCar() {

            String carName = CarType.FIAT.getName();
            int speed = CarType.FIAT.getSpeed();
            String symbol = CarType.FIAT.getSymbol();
            System.out.println(carName + " " + speed + " "+ symbol);
            return new Fiat(carName, speed, symbol);

}
}
