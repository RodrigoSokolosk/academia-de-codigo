package org.academiadecodigo.carcrash.cars;

public enum CarType {
    FIAT("Fiat", 30, "F"),
    MUSTANG("Mustang", 100, "M");

    private String name;
    private int speed;
    private String symbol;

    CarType(String name, int speed, String symbol) {
        this.name = name;
        this.speed = speed;
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public int getSpeed() {
        return speed;
    }

    public String getSymbol(){
        return symbol;
    }
}