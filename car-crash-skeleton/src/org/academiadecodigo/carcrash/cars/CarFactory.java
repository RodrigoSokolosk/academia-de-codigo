package org.academiadecodigo.carcrash.cars;

public class CarFactory{
        //private Car newCar;
        private String name;
        private int speed;
        private String symbol;

    public CarFactory(String name, int speed, String symbol){
        super();
        this.name = name;
        this.speed = speed;
        this.symbol = symbol;

    }
    public static Car getNewCar() {
        String carName = "";
        int speed = 0;
        String symbol = "";
        int type = (int)Math.ceil(Math.random()*2);


        if(type==1){
            carName = CarType.FIAT.getName();
            speed = CarType.FIAT.getSpeed();
            symbol = CarType.FIAT.getSymbol();
            System.out.println(carName + " " + speed + " "+ symbol);
            type++;
            return new CarFactory(carName, speed, symbol);
        }
        if(type==2){
            carName = CarType.MUSTANG.getName();
            speed = CarType.MUSTANG.getSpeed();
            symbol = CarType.MUSTANG.getSymbol();
            System.out.println(carName + " " + speed + " " + symbol);
            type++;
            return new CarFactory(carName, speed, symbol);
        }
        //type++;
        return new CarFactory(carName, speed, symbol);
    }
    /*@Override
    public String toString(){
        return this.symbol;
    }*/
}
