package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

abstract public class Car {

    /**
     * The position of the car on the grid
     */
    private Position pos;
    private int posRow;
    private int posCol;

    public Car() {
        this.posRow = (int) (Math.random() * 25);
        this.posCol = (int) (Math.random() * 100);
        this.pos = new Position(posCol, posRow);

    }

    public Position getPos() {
        return pos;
    }

    public void setPos(){
        this.pos = new Position(posCol, posRow);
        int casePos =  (int) (Math.random() * 4);
        switch (casePos){
            case 1: moveBack();
            break;
            case 2: moveFoward();
            break;
            case 3: moveDown();
            break;
            case 4: moveUp();
            break;
        }
    }
    public boolean isCrashed() {
        return false;
    }
    public int moveFoward(){
       return posCol++;
    }
    public int moveBack(){
        return posCol--;
    }
    public int moveUp(){
        return posRow--;
    }
    public int moveDown(){
        return posRow++;
    }

    @Override
    public abstract String toString();

}
