package org.academiadecodigo.shellmurais;

public class Main {

    public static void main(String[] args) {

        MonoOperation<Integer> monoOperation = (x) -> x + 2;
        System.out.println(monoOperation.operation(5));

        BiOperation<String> biOperation = (x, y) -> x + y;
        System.out.println(biOperation.operation("Hello ", "World!!"));

        Person person = new Person("Rui");
        System.out.println(person.getName());

        MonoOperation<String> changeName = (name) -> {
            person.setName(name);
            return name;
        };
        changeName.operation("Stevenson");
        System.out.println(person.getName());

        OperationMachine<Integer> integerMachine = new OperationMachine<Integer>();

        BiOperation<Integer> biOperation1 = (i1, i2) -> i1 + i2;
        Integer result = integerMachine.perform(biOperation1, new Integer(10), new Integer(5));
        System.out.println(result);

    }
}
