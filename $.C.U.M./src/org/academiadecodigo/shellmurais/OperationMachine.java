package org.academiadecodigo.shellmurais;

public class OperationMachine <T> {

    T perform(MonoOperation<T> monoOperation, T param){
        return monoOperation.operation(param);
    }

    T perform(BiOperation<T> biOperation, T param, T param2){
        return biOperation.operation(param, param2);
    }
}
