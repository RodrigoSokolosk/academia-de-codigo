package org.academiadecodigo.shellmurais;

public interface MonoOperation<T> {

     T operation(T x);

}
