package org.academiadecodigo.shellmurais;

public interface BiOperation<T> {

    T operation(T x, T y);

}
