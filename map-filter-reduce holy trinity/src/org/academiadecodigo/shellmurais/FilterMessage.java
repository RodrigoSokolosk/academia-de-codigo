package org.academiadecodigo.shellmurais;

import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilterMessage {


    public String filteringMessage(String message){
        Stream<String> messageStream = Stream.of(message.split(" "));
        String filteredMessage = messageStream.filter(name -> !name.equals("garbage")).map(name -> name.toUpperCase()).reduce("", (acc, element) -> acc + " " + element);
        System.out.println(filteredMessage);
        return filteredMessage;
    }
}
